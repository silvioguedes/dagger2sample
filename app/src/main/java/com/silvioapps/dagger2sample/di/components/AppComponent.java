package com.silvioapps.dagger2sample.di.components;

import android.app.Application;

import com.silvioapps.dagger2sample.di.modules.AppModule;
import com.silvioapps.dagger2sample.features.shared.applications.CustomApplication;

import javax.inject.Singleton;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

/**
 * Created by silvio on 19/08/17.
 */

@Singleton
@Component(modules = {AndroidInjectionModule.class, AppModule.class})
public interface AppComponent {

    @Component.Builder
    interface Builder{
        @BindsInstance Builder application(Application application);
        AppComponent build();
    }

    void inject(CustomApplication application);
}
