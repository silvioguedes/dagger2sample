package com.silvioapps.dagger2sample.di.modules;

import android.app.Application;
import android.content.Context;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;

/**
 * Created by silvio on 19/08/17.
 */

@Module(includes = {ActivitiesModule.class, FragmentsModule.class})
public class AppModule{

    //provê o Context que a classe HelloWorld necessita
    @Provides
    @Singleton
    public Context providesContext(Application application){
        return application;
    }
}

