package com.silvioapps.dagger2sample.di.modules;

import com.silvioapps.dagger2sample.features.activities.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by silvio on 19/08/17.
 */

@Module
public abstract class ActivitiesModule {
    @ContributesAndroidInjector
    abstract MainActivity contributesMainActivity();
}
