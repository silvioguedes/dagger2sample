package com.silvioapps.dagger2sample.di.modules;

import com.silvioapps.dagger2sample.features.fragments.MainFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by silvio on 19/08/17.
 */

@Module
public abstract class FragmentsModule {
    @ContributesAndroidInjector
    abstract MainFragment contributesMainFragment();
}
