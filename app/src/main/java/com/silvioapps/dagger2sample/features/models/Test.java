package com.silvioapps.dagger2sample.features.models;

import javax.inject.Inject;

/**
 * Created by silvio on 21/08/17.
 */

public class Test {
    private HelloWorld helloWorld = null;

    @Inject
    public Test(HelloWorld helloWorld){
        this.helloWorld = helloWorld;
    }

    public void foo(){
        helloWorld.show();
    }
}
