package com.silvioapps.dagger2sample.features.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.silvioapps.dagger2sample.R;
import com.silvioapps.dagger2sample.features.models.Test;

import javax.inject.Inject;
import dagger.android.support.AndroidSupportInjection;

public class MainFragment extends Fragment {
    @Inject//injetará a instância de HelloWorld em Test e injetará a instância de Context em HelloWorld
    Test test;

    @Override
    public void onAttach(Context context){
        AndroidSupportInjection.inject(this);//importante adicionar e antes do super

        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle){

        test.foo();//método foo() irá chamar o método show() do HelloWorld

        return layoutInflater.inflate(R.layout.fragment_main, viewGroup, false);
    }
}
