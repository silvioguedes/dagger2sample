package com.silvioapps.dagger2sample.features.models;

import android.content.Context;
import android.widget.Toast;
import javax.inject.Inject;

/**
 * Created by silvio on 21/08/17.
 */

public class HelloWorld {
    private Context context = null;

    @Inject
    public HelloWorld(Context context){
        this.context = context;
    }

    public void show(){
        Toast.makeText(context, "Hello World!",Toast.LENGTH_LONG).show();
    }
}
