package com.silvioapps.dagger2sample.features.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.silvioapps.dagger2sample.R;
import com.silvioapps.dagger2sample.features.fragments.MainFragment;
import com.silvioapps.dagger2sample.features.models.Test;

import javax.inject.Inject;
import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;

public class MainActivity extends AppCompatActivity implements HasAndroidInjector {
    @Inject//injetará a instância de HelloWorld em Test e injetará a instância de Context em HelloWorld
    Test test;
    @Inject
    DispatchingAndroidInjector<Object> fragmentDispatchingAndroidInjector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);//importante adicionar e antes do super

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //se quiser testar na activity
        //test.foo();//método foo() irá chamar o método show() do HelloWorld

        if(savedInstanceState == null) {
            attachFragment(R.id.frameLayout, new MainFragment());
        }
    }

    @Override
    public AndroidInjector<Object> androidInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    protected void attachFragment(int resId, Fragment fragment){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment fragmentById = getSupportFragmentManager().findFragmentById(resId);
        if(fragmentById == null && !isFinishing()){
            fragmentTransaction.add(resId, fragment);
            fragmentTransaction.commit();
        }
    }
}
